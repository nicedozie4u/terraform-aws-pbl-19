region = "us-east-1"

vpc_cidr = "10.0.0.0/16"


enable_dns_support = "true"

enable_dns_hostnames = "true"

preferred_number_of_public_subnets = 2

preferred_number_of_private_subnets = 4

environment = "dev"

ami-web = "ami-052f5d137f2d5c7e0"

ami-bastion = "ami-0e2a6aa512d2b4b77"

ami-nginx = "ami-0418a842cfe3adb1f"

ami-sonar = "ami-0d3b3319be640746a"

keypair = "project19"

master-password = "devopspblproject"

master-username = "dozie"

account_no = "127560435092"

tags = {
  Owner-Email     = "nicedozie4u@yahoo.com"
  Managed-By      = "Terraform"
  Billing-Account = "127560435092"
}